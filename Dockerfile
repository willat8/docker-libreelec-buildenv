FROM ubuntu:devel

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    gcc \
    make \
    git \
    unzip \
    wget \
    xz-utils \
    python3 \
    bc \
    patchutils \
    gawk \
    gperf \
    zip \
    lzop \
    g++ \
    xfonts-utils \
    xsltproc \
    default-jre \
    libncurses5-dev \
    libjson-perl \
    libxml-parser-perl \
 && rm -rf /var/lib/apt/lists/*

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1 \
 && useradd -g users -m -s /bin/bash builduser

USER builduser
WORKDIR /home/builduser/LibreELEC.tv

CMD PROJECT=RPi DEVICE=RPi4 ARCH=arm TMPDIR=$(pwd)/tmp make -j3 image

